from PIL import Image

image_path = "../src/static/images/"
src_img = Image.open(image_path + "plane.png")

target_img = Image.new('RGBA', (32, 32))

for angle in range(0, 360, 5):
    target_img = Image.new('RGBA', (40, 40))
    im = src_img.convert('RGBA')
    rot = im.rotate(-angle, expand=1)
    target_img.paste(rot, (0, 0), rot)
    target_img.save(image_path + "plane" + str(angle) + ".png")
