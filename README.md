# Flightsim Tracker #

Flightsim tracker is set up as a demo project. WHile writing a book about full stack development we want to integrate all aspects of software development.
The project is hosted on Atlassian on https://bitbucket.org/RobertLos/flightsim-tracker

## Documentation ##
Well, this readme is the first documentation and should act as a lead for all subsequent development, installation, etc. For now it is only this file, but that will change rapidly
I hope.
Documentation comes as:
### This readme ###
This readme, well you are looking at it :-)
### Pydoc, javadoc ###
In source documentation
### Build reports ###
Yes we use Jenkins
### The Full stack devops book ###
Well, we are writing...
### Various websites
https://ngmap.github.io

## How far are we ##
We have a basic working application which connects to X-Plane and shows a map on screen. Loads of improvements
to be made though.

## License ##
During development we maintain it closed, but when the first publications go live and there is actually something to show we will bring the source under an open source
license. We will also disclose all sources than as part of the book about full stack development we are currently writing.