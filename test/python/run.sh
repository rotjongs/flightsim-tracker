#!/bin/bash

# Run script for docker to start the unit testing, coverage and code style checking of PHP components
# author: Robert Los


DEBUG="off"
BRAINSTAM_NS_PATH=$(readlink -e `dirname $PYTHON_EXE`/..)/$BRAINSTAM_NS
touch $BRAINSTAM_NS_PATH
if [ -z "$PYTHONPATH" ]; then
    PYTHONPATH=$WORKSPACE/src/BRAINSTAM
else
    PYTHONPATH=$WORKSPACE/src/BRAINSTAM:$PYTHONPATH
fi

WORKSPACE=`pwd`
PYTHONCODE_DIR=src


repair_relative_paths() {
    echo "************  Repair wrong path names from /${PYTHONCODE_DIR} to ${PYTHONCODE_DIR}"
    pushd ${WORKSPACE}test_results
    sed -i -- 's/\/${PYTHONCODE_DIR}/${PYTHONCODE_DIR}/g' *.xml
    popd
}

usage () {
   echo "usage: $0 [option]"
   echo "options are: "
   echo "  -a | --all           perform all tests"
   echo "  -d | --debug"
   echo "  -c | --coverage      determine unit testing report"
   echo "  -h | --help          print this usage"
   echo "  -s | --style         perform code style testing"
   echo "  -u | --unit-test     perform unit testing"

   if [[ "$DEBUG" = "on" ]]; then
        echo "PYTHONPATH=$PYTHONPATH"
   fi

}

wipe_old_results() {
    echo "cleaning ${WORKSPACE}test_results/*"
    rm -f ${WORKSPACE}test_results/*
}

set_debug() {
   DEBUG="on"

    if [[ "$DEBUG" = "on" ]]; then
        echo "debug is now on..."
    fi
}

unit_test () {
    echo "*** Unit testing with py.test starting in `pwd` ..."
    find . -name '*.pyc' -exec rm -rf {} \;
    py.test -s --verbose --junitxml=${WORKSPACE}test_results/py-unittest-results.xml --confcutdir=`pwd`
    if [[ "$DEBUG" = "on" ]]; then
        echo "completed unit test with return code $?"
    fi
}

coverage () {
    echo "*** Coverage of Unit testing with py.test now starting in `pwd` ..."
    py.test \
            --cov=src --cov-report=xml
    find $WORKSPACE -name 'coverage.xml' -exec mv "{}" ${WORKSPACE}test_results/py-coverage.xml \;
    if [[ "$DEBUG" = "on" ]]; then
        echo "completed coverage with return code $?"
    fi
}

code_style () {
    echo "*** Code style checking with flake8 now starting in `pwd` ... "
    python -m flake8 \
      --output-file ${WORKSPACE}test_results/py-violations.txt
    sed -i -- 's/^.\//src\//g' ${WORKSPACE}test_results/py-violations.txt
    if [[ "$DEBUG" = "on" ]]; then
        echo "completed code style assessment with return code $?"
    fi
}


cd src
echo "processing $# arguments: $0 $@"

if [[ $# == 0 ]]; then
    usage
fi

for action in "$@"
do
    echo "processing $action"
    wipe_old_results
    case "$action" in
      -u|--unit-test)
        unit_test
      ;;
      -c|--coverage)
        coverage
      ;;
      -s|--style)
        code_style
      ;;
      -a|--all)
        unit_test
        coverage
        code_style
      ;;
      -h|--help)
        usage
        break
      ;;
      -d|--debug)
        set_debug
      ;;
      *)
        echo "Error: unknown option"
        usage
        exit 1
      ;;
    esac
done
repair_relative_paths