#!/usr/bin/env bash

# wrapper to generate and run a docker container for unit and codestyle checking of Python code

# generate a random docker tag to prevent two dockers running the same image
dockertag=`gpw 1 16`

# build the docker
docker build -t ${dockertag} -f test/python/Dockerfile .
active_docker_image=`docker images -q | head -1`

echo "docker image used: ${active_docker_image} on ${dockertag}"

# run the tester and store the results in test_results which will be mounted
docker run --rm -v $(pwd)/test_results:/test_results -v $(pwd)/src:/src ${dockertag} /run.sh $@

