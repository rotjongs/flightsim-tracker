/**
 * Created by Robert Los on 24-7-2016
 */

var FlightSimApp = angular.module('FlightSimApp', ['ngMap']);

FlightSimApp.controller('ChartController', function($scope, $http, $interval){
    $scope.teststring = 'Howdy, This is a test in monitoring an X-Plane session on a web page either on the same computer ' +
        'or on another one in the same network. It will display a map centered around the current plane and will ' +
        'display several things below:';

    $scope.map = {
        zoom: 9,
        options:{
            mapTypeID: 'TERRAIN'
            },
    };

    $http.get("/aircraft")
        .then(function(response) {
            $scope.aircraft = response.data.aircraft;
        });

    $http.get("/position")
        .then(function(response) {
            $scope.position = response.data.position;
        });

    $interval(function(){
        $http.get("/position")
        .then(function(response) {
            $scope.position = response.data.position;
        })
    }, 500);
});

FlightSimApp.controller('InfoController', function($scope){
    $scope.copyright = 'BrainStam.nl';
    $scope.features = [
        'Current heading and speed',
        'Flight plan (when available)',
        'Recorded track',
        'Current fuel state - and implication to flight plan'
    ];

    $scope.sites = [
        'www.brainstam.nl',
        'www.x-plane.com'
    ];
});

