# import xpc

"""
Tracklog.py is started as a thread and records the entire flight. The flight is recorded in a sql database
which can be configured using SQLalchemy. So it can be mysql, sqlite, oracle or any other sql based database

Tracklog is a class which has methods like:
    init-log
    get-last-position
    start-segment
    dump-segment
    reload-segment

calling functions can load the last segment (or segment since last call) and display that on screen
"""


class TrackLog():
    pass
