from flask import Flask, request
from flask_socketio import SocketIO
import os
import json
import xpc
import socket
import time
# from tracklog import TrackLog

app = Flask(__name__, static_url_path='')
socket_io = SocketIO(app)

_VERSION = '0.1-SNAPSHOT'


@app.route('/')
def main_index():
    """
    Since this is a one page application the only route is modeled here.
    :return: static file to display
    """
    notify_connection(request)
    return app.send_static_file('index.html')


@app.route('/smoketest')
def smoke_test():
    """
    Just to test that the app is available
    :return: json string with the version
    """
    app_array = {'flightsim_tracker': {
        "version": _VERSION,
        "status": 'up'
    }
    }
    return json.dumps(app_array)


@app.route('/sim_position')
def sim_pos():
    """
    simulate x-plane and return a static position (for development)
    :return: json dump of position
    """
    pos = {'position': {
        'latitude': '51.963309',
        'longitude': '4.501972',
        'altitude': '-2',
        'image': {'url': 'images/plane-yellow.png',
                  'size': (32, 32),
                  'anchor': (16, 16),
                  'origin': (0, 0),
                  'rotation': 34
                  }
        }
    }
    return json.dumps(pos)




def round5(x, base=5):
    return int(base * round(float(x) / base))


@app.route('/position')
def position():
    """
    Micro webservice to report the position acquired from x-plane
    :return: json string of position or the home address of the author
    """
    try:
        with xpc.XPlaneConnect() as client:
            current_position = client.getPOSI()
        heading = current_position[5]
        plane_image = 'images/plane' + str(round5(heading)) + '.png'
        pos = {'position': {
            'latitude': current_position[0],
            'longitude': current_position[1],
            'altitude': current_position[2],
            'image': {'url': plane_image,
                      'size': (32, 32),
                      'anchor': (16, 16),
                      'origin': (0, 0)
                      },
            'rotation': 30,
            'lat_str': str_pos(current_position[0]),
            'lon_str': str_pos(current_position[1]),
            'alt_str': str(int(current_position[2] / 0.3048)) + " ft"
            }
        }
    except socket.timeout:
        pos = json.loads(sim_pos())
    return json.dumps(pos)


@app.route('/aircraft')
def aircraft():
    """
    Gert more information about the plane
    :return: http response
    """

    try:
        with xpc.XPlaneConnect() as client:
            dref = 'sim/aircraft/view/acf_ICAO'
            ac_type = float_to_string(client.getDREF(dref))

            dref = 'sim/aircraft/view/acf_tailnum'
            tail_num = float_to_string(client.getDREF(dref))

        airplane = {'aircraft': {
            'icao': ac_type,
            'tailnum': tail_num
            }
        }

    except socket.timeout:
        airplane = {'aircraft': {
            'icao': 'XX',
            'tailnum': 'PH-LOS'
            }
        }
    return json.dumps(airplane)


def float_to_string(infloat):
    ret_str = ""
    for i in infloat:
        if not int(i):
            break
        ret_str += chr(int(i))
    return ret_str


def notify_connection(req):
    """
    write a message on the screen of the flight simulator
    that a connection has been made.
    :param req: string to be displayed
    :return:
    """
    try:
        with xpc.XPlaneConnect() as client:
            client.sendTEXT("%s connected to Flightsim Tracker" % req.remote_addr)
            time.sleep(3)
            client.sendTEXT()
        return True
    except socket.timeout:
        return False


def str_pos(pos):
    d = int(pos)
    md = abs(pos - d) * 60
    m = int(md)
    sd = (md - m) * 60
    return "%d %d' %2.1f\"" % (d, m, sd)


if __name__ == '__main__':
    socket_io.run(app, host=os.getenv('IP', '0.0.0.0'), port=int(os.getenv('PORT', 5050)), debug=False)
