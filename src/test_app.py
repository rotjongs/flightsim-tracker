from src.app import app, float_to_string, round5, sim_pos, str_pos

import json


def test_smoketest():
    """
    Test that we can do a smoketest on the web application
    :return:
    """
    tester = app.test_client()
    response = tester.get('/smoketest')
    assert response.status_code == 200, "Access to smoketest failed"
    smoke_result = json.loads(response.data)
    assert smoke_result.get('flightsim_tracker').get('version') == '0.1-SNAPSHOT'
    assert smoke_result.get('flightsim_tracker').get('status') == 'up'


def test_sim_position():
    """
    Test that we can read a simulated position with the right altitude of -2
    :return:
    """
    tester = app.test_client()
    response = tester.get('/sim_position')
    assert response.status_code == 200, "Access to simulated position failed"
    pos = json.loads(response.data)
    assert int(pos.get('position').get('altitude')) == -2, "Altitude must be in the response"


def test_sim_pos():
    """
    Test the bare function without going through web frontend
    :return:
    """
    pos = json.loads(sim_pos())
    assert int(pos.get('position').get('altitude')) == -2, "Altitude must be in the response"


def test_connection():
    tester = app.test_client()
    response = tester.get('/')
    assert response.status_code == 200, "Access to main page not OK"


def test_float_to_string():
    instr = (49.0, 46.0, 51.0)
    assert "1.3" == float_to_string(instr), "Return floats from flight sim can not be displayed"


def test_round5():
    assert round5(23) == 25, "should be rounded to the nearest value"
    assert round5(20, 4) == 20, "should be rounded to the nearest value"
    assert round5(356) == 355, "should be rounded to the nearest value"


def test_str_pos():
    assert str_pos(4.50) == "4 30' 0.0\""
